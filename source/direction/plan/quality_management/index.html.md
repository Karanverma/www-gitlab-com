---
layout: markdown_page
title: "Category Vision - Quality Management"
---

- TOC
{:toc}

## Quality Management
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

Many organizations manage quality through both manual and automated testing. These
are supported by artifacts such as test plans, test cases, and test execution runs,
and having them integrate back with documented issues and feature requirements,
allowing for feature-test traceability and quantifying that coverage. Quality management
in GitLab addresses these problems and use cases.

See [issues for quality management](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=✓&state=opened&label_name[]=quality%20management).

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, test suites, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself.

The MVC is https://gitlab.com/groups/gitlab-org/-/epics/617.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Competitors in this space include qTest and HPQC (HP Quality Center). They are focused on managing test cases as part of the software development lifecycle. Our approach and response will be to have similar basic test case management features (i.e. test objects), and then quickly move horizontally to integrate with other places in GitLab, such as issues and epics and even requirements management. See https://gitlab.com/groups/gitlab-org/-/epics/670. With this strategy, we would not be necessarily competing directly with these existing incumbents, but helping users with the integration pains of multiple tools and leveraging other, more mature areas of GitLab as we iterate.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

We have yet to engage more closely with analysts in this area. As this product category is prioritized for improvements as our Plan product and engineering headcount grows, we expect to engage more with analysts.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

Few GitLab customers or prospects have asked about Quality Management. But the few that have, are asking about best practices and how they can possibly use GitLab and not worry with another tool. We are considering https://gitlab.com/groups/gitlab-org/-/epics/617 for that very purpose.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Similar to above, the top user issue (epic) is https://gitlab.com/groups/gitlab-org/-/epics/617.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

We continue to work with GitLab's Quality Team to scope out the MVC of quality management. See https://gitlab.com/groups/gitlab-org/-/epics/617.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->
- MVC: https://gitlab.com/groups/gitlab-org/-/epics/617
