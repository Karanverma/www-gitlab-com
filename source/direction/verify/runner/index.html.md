---
layout: markdown_page
title: "Category Vision - Runner"
---

- TOC
{:toc}

## GitLab Runner

The GitLab Runner is our execution agent that works with [GitLab CI](/direction/verify/continuous_integration)
to execute the jobs in your pipelines.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

### The Custom Executor

A common request we get is to add support for various platforms to the Runner. We've chosen to support
the plethora of different systems out there by recently adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html)
that can be overridden to support different needs. In this way platforms like Lambda, z/OS, vSphere, and
even custom in-house implementations can be supported in the runner for your needs.

## What's Next & Why

Up next is adding [interactive web terminal](https://docs.gitlab.com/ee/ci/interactive_web_terminal/) support for the Runner into the Helm Chart
([gitlab-runner#3859](https://gitlab.com/gitlab-org/gitlab-runner/issues/3859)). This
will help more users take advantage of this great feature.

At the same time we are working on making the runner extensible, introducing
a shim for the core runner capabilites via [gitlab-runner#2885](https://gitlab.com/gitlab-org/gitlab-runner/issues/2885)
which will make it possible for teams to support alternative provisioning
models, archictures, and for runtime injection of behaviors into the runner.

## Maturity Plan

Since this category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)),
we do not have an upcoming target for bringing it to the next level. It's important to us that
we defend our lovable status, though, so if you see any actual or potential gap please let us know in
our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/1546) for this category.

Maturing the Windows executor is of particular importance as we improve our support for Windows
across the board. There are a few issues we've identified as being critical to that effort:

- [Support named pipes for Windows Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/4295)
- [Support services on Windows Docker containers](https://gitlab.com/gitlab-org/gitlab-runner/issues/4186)
- [Use Docker Save to publish docker helper images](https://gitlab.com/gitlab-org/gitlab-runner/issues/3979)
- [Distribute Docker windows image for gitlab-runner](https://gitlab.com/gitlab-org/gitlab-runner/issues/3914)
- [Add support for Windows device for Docker executor](https://gitlab.com/gitlab-org/gitlab-runner/issues/3923)

## Competitive Landscape

For the moment, the Runner is evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned
processes can cause issues with the runner in certain cases has been highlighted as generating
support issues.

## Top Customer Issue(s)

We have a few top issues that we're investigating:

- [gitlab-ee#63858](https://gitlab.com/gitlab-org/gitlab-ce/issues/63858): Allow runners to push via their CI tokens
- [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797): Local runner execution
- [gitlab-runner#2229](https://gitlab.com/gitlab-org/gitlab-runner/issues/2229): Adding Services With Kubernetes Executor
- [gitlab-runner#2007](https://gitlab.com/gitlab-org/gitlab-runner/issues/2007): Variables nested substitution
- [gitlab-runner#1736](https://gitlab.com/gitlab-org/gitlab-runner/issues/1736): File/directory creation umask when cloning is `0000`

## Top Internal Customer Issue(s)

We have an issue with timeouts not working that is causing issues for several users ([gitlab-runner#4147](https://gitlab.com/gitlab-org/gitlab-runner/issues/4147)),
and it is on our agenda to resolve.

## Top Vision Item(s)

We frequently receive requests for supporting different provisioning platforms and
architectures in the runner - we haven't been able to support these because each combination
brings in additional requirements for testing and maintenance. Instead, we want to enable
people to build their own integration steps into the runner in a safe way. Through introducing
a shim for the core runner capabilites in [gitlab-runner#2885](https://gitlab.com/gitlab-org/gitlab-runner/issues/2885)
we make this possible for our users.

Additionally, supporting prioritization of runners via [gitlab-ce#63860](https://gitlab.com/gitlab-org/gitlab-ce/issues/63860)
could be very powerful. For example, you could have multiple runners but have a few more powerful ones
that you want to be selected first if they are available.
