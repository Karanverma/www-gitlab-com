---
layout: job_family_page
title: "GitLab.com Support"
---

## GitLab.com Support Roles at GitLab

GitLab.com Support Agents focus on providing support for customers and users of GitLab.com. They are the first point of contact when a customer has trouble. They should be cool under pressure, comfortable with technology, and always willing to go the extra mile to make things right.

### Intermediate GitLab.com Support Agent

- Engage with our customers to triage customer issues via email
- Focus on efficiency
- Collaborate with Support Engineers and the GitLab Community to get bugs fixed
- Create or update internal documentation on support processes
- Maintain good ticket performance and satisfaction
- Have a good understanding of how SaaS software works
- Desire to dive in and understand problems
- Driven to understand new challenges
- Customer focused always
- Excellent spoken and written English
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Requirements

 - 2+ years in a customer facing / customer service role, ideally in a technical capacity.
 - Experience with Git / source control and/or the software development lifecycle.
 - Customer oriented individual; has the ability to adapt and respond to different kinds of characters.
 - Good problem solving skills that result in quality resolutions.

## Nice to haves

 - Previous experience in a startup or environment that is scaling would be an added benefit for this role.
 - Familiaritity with terms like 'CI/CD', 'test-driven development', 'Docker registry' or '2FA'


### Senior GitLab.com Support Agent

This role expands on the duties of an Intermediate GitLab.com Support Agent.

- Come up with a catchier name for yourself
- Act as a mentor to train more Senior GitLab.com Support Agents
- Build efficient processes
- Possess Expert Level SaaS Debugging
- Have a deep desire to understand how software works
- Liaise with other teams echoing the customer voice
- Coordinate with Infrastructure and Production teams to build collaborative workflows

## Performance Indicators

Gitlab.com Support Agents have the following job-family performance indicators.

* [Customer satisfaction with Support](/handbook/support/#support-satisfaction-ssat)
* [Monthly tickets solved](/handbook/support/#average-monthly-tickets-per-support-team-member)
* [Service Level Agreement](/handbook/support/#service-level-agreement-sla)
* [Ticket deflection through documentation updates](/handbook/support/#ticket-deflection-through-documentation)


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting an interview, find their job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a 30min [screening call](/handbook/hiring/interviewing/#screening-call) with our Global Recruiters
* A GitLab Team member from any team will conduct a behavioral interview to get a sense of your work experience.
* Candidates will schedule an interview with our Support Lead
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
