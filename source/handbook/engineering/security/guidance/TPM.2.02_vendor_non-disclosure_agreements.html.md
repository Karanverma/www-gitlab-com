---
layout: markdown_page
title: "TPM.2.02 - Vendor Non-Disclosure Agreement Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# TPM.2.02 - Vendor Non-Disclosure Agreement

## Control Statement

Requirements for confidentiality or non-disclosure agreements reflecting the organization’s needs for the protection of information shall be identified, regularly reviewed and documented.

## Context

This control provides GitLab a written, signed agreement with a vendor that non-disclosure of confidential information will be disseminated to outside parties.

## Scope

This control applies to all information shared with third parties that interact with the GitLab production environment.

## Ownership

Control Owner:

* Legal

Process Owner:

* Procurement

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.2.02_vendor_non-disclosure_agreements.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.2.02_vendor_non-disclosure_agreements.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/TPM.2.02_vendor_non-disclosure_agreements.md).

## Framework Mapping

* ISO
  * A.13.2.2
  * A.14.2.7
  * A.15.1.1
  * A.15.1.2
  * A.15.1.3
  * A.15.2.2
* PCI
  * 12.8.2
