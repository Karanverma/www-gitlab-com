---
layout: markdown_page
title: "AM.1.01 - Inventory Management Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# AM.1.01 - Inventory Management

## Control Statement

GitLab maintains an inventory of system devices, which is reconciled quarterly.

## Context

The purpose of this control is to ensure we are monitoring the systems in use by GitLab. We can't prove we are protecting all GitLab systems if we don't have an up-to-date inventory of those systems.

## Scope

This control applies to all GitLab endpoint workstations as well as virtual assets within our hosting providers.

## Ownership

The GitLab IT Operations and Infrastructure teams are the primary owners of this control.

IT Operations owns workstation assets.

Infrastructure owns server/virtual host-based assets.

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/AM.1.01_inventory_management.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/AM.1.01_inventory_management.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/AM.1.01_inventory_management.md).


## Framework Mapping

* ISO
  * A.8.1.1
* PCI
  * 9.6.1
  * 9.7
  * 9.7.1
