---
layout: markdown_page
title: "IAM.2.02 - Password Authentication Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.2.02 - Password Authentication

## Control
User and device authentication to information systems is protected by passwords that meet GitLab's [password policy guidelines](https://about.gitlab.com/handbook/security/#gitlab-password-policy-guidelines). For systems involved in payment card processing, GitLab requires those systems and system users to change passwords quarterly.

## Context

By ensuring passwords are implemented when and where appropriate, sensitive and valuable data is protected from unauthorized access and use. Enforcing GitLab's password complexity requirements further protects that data by reducing the risk of brute force and dictionary attacks that aim to guess user passwords.

## Scope

This control applies to any system or service where password protection is appropriate.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLab team-members, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.2.02_password_authentication.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.2.02_password_authentication.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.2.02_password_authentication.md).

## Framework Mapping

* ISO
  * A.9.1.2
  * A.9.4.1
  * A.9.4.2
  * A.9.4.3
* SOC2 CC
  * CC6.1
  * CC6.6
  * CC6.7
* PCI
  * 8.2
  * 8.2.3
  * 8.2.4
  * 8.2.5
  * 8.2.6
  * 8.6
