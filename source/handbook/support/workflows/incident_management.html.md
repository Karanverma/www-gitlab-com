---
layout: markdown_page
title: Incident Management for GitLab.com Support
category: GitLab.com
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

This workflow outlines the support team's role in GitLab.com Incident Management as well as guidelines for initiating an incident.

## What is an incident?

From [our engineering handbook](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/), incidents are **anomalous conditions** that result in **service degradation** or **outages** and require human or automated intervention to restore service to full operational status in the shortest amount of time possible.

### Is GitLab.com Experiencing an Incident?

If you're observing issues on GitLab.com or working with users who are reporting issues, please follow the instructions found on the [On-Call](/handbook/on-call/) page and alert the Engineer On Call (EOC).

Generally, issues rated ~S1 or ~S2 should be immediately considered for incident management. However, it's in the best interest of our users and the reliability of GitLab.com that you err on the side of caution and ask if unsure.

| Label | Meaning           | Functionality                                         | Affected Users                   | GitLab.com Availability                            | Performance Degradation      |
|-------|-------------------|-------------------------------------------------------|----------------------------------|----------------------------------------------------|------------------------------|
| ~S1   | Blocker           | Unusable feature with no workaround, user is blocked  | Impacts 50% or more of users     | Outage, Significant impact on all of GitLab.com    |                                                       |
| ~S2   | Critical Severity | Broken Feature, workaround too complex & unacceptable | Impacts between 25%-50% of users | Significant impact on large portions of GitLab.com | Degradation is guaranteed to occur in the near future |

#### Examples

- A few reports of being unable to access merge requests ballooned to over 20,000 exceptions logged, leading to an incident. ([gitlab-ce#63030](https://gitlab.com/gitlab-org/gitlab-ce/issues/63030))
- A single report of not being able to access GitLab.com groups and broken pipelines led to an executive escalation, then an incident. ([gitlab-ee#11704](https://gitlab.com/gitlab-org/gitlab-ee/issues/11704))

