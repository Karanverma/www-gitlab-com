---
layout: markdown_page
title: Subscription and billing issues
category: License and subscription
---

## On This Page
{:.no_toc}

- TOC
{:toc}

----
## Overview
Refer to this page when a user has questions/issues related to transactions, licensing or billing for self-managed or GitLab.com. Note the information on this page applies to both Self-Managed and GitLab.com users/products unless specifically indicated as an exception.

The list of common requests/issues below is reference for internal support agents. Unless specifically noted, utilize the following issue tracker decision table for submitting issues.

| Issue Tracker |Use Case | 
|------|-------|
| [Community Edition](https://gitlab.com/gitlab-org/gitlab-ce/issues) | Issue is caused by or affecting a core feature  |
| [Enterprise Edition](https://gitlab.com/gitlab-org/gitlab-ee/issues) |   Issue is caused by or affecting a paid feature  |  
| [Support Engineer Escalations](https://gitlab.com/gitlab-com/support/dotcom/dotcom-escalations/issues) | Customers portal issue which may be billing or bad data related    |
| [Customers Portal](https://gitlab.com/gitlab-org/customers-gitlab-com/issues) | Issue is caused specifically by something within the Customers Portal    |   



## Transactions
Transactions at GitLab are defined as anything related to purchasing; issues or questions. For example. credit card problems, bugs, trying to make a purchase and running into confusion/problems etc. 



1. **User reports an inability to upgrade from one paid plan to another.** Currently users cannot upgrade their subscription from the customers portal, although an [enhancement for this is coming soon](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/516). If a user wants to upgrade from one paid plan to another, utilize the `Upgrade Plan Request` macro in Zendesk. This will request necessary information from the user and reassign the ticket to the upgrades and renewals queue.

1. **A GitLab.com user is on a trial and wants to purchase a paid plan.** While a GitLab.com user subscribes to a trial, they are not currently able to purchase a subscription in the customers portal, although an [enhancement for this is coming soon](https://gitlab.com/gitlab-org/gitlab-ee/issues/12187). If a user wants to purchase a paid plan while subscribed to a trial, the user should submit a ticket to GitLab.com support and a member of the support team will update the expiration date of the trial to the end of the day UTC. Once expired, the user will be able to purchase the subscription from the customers portal. As a more immediate solution if necessary, the support team can submit a [Support Engineer issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-escalations/issues/new) to request the immediate expiration of the trial through the console.

1. **User wants to trial a plan other than Gold on GitLab.com.** GitLab.com [only offers the self-service ability to trial the Gold subscription](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/409). With manager approval, GitLab.com support can assist with trials of other plans. Ask the user to create the GitLab.com Gold trial, then submit a [Plan Change request issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=). GitLab.com support with manually change the plan from Gold to the desired plan and revert it back to the original plan at the expiration of the trial.

1. **User receives an error during the purchasing process within the [customers portal](https://customers.gitlab.com/customers/sign_in).** When purchasing a subscription via the customers portal if a user receives an error about an address or credit card, check the address listed in the `My Account` section of the customers portal. This is typically due to a bad address (city/state/zip code). If the address is correct and the error persists, submit [an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-escalations/issues) and utilize the SE Escalation template. A support engineer will rule out if this is a billing or bad data issue and advise if a [customers portal issue](https://gitlab.com/gitlab-org/customers-gitlab-com/issues) is needed.

1. **User wants to downgrade subscription.** There is currently [no ability to downgrade a subscription from a self-service perspective](https://gitlab.com/gitlab-org/customers-gitlab-com/issues/368). If a user wants to downgrade and they are within 45 days of the purchase, send the request to the AR team by selecting the form `Accounts Receivable` in Zendesk and advise the user to purchase the desired plan once cancelled/refunded. If the user is outside of the 45 day period, advise them that we can cancel the purchase but the subscription will not be refunded. In this case you can also pass the issue to the assigned Account Manager in sales.

1. **User wants the red renewal approaching banner message in their Self-Managed system removed.** This message will be displayed to all users in the 30 days leading up to the renewal date. The message will only be removed when a new license key is uploaded.

1. **User doesn't know the steps to purchase a GitLab.com subscription.** 
   
   - Create an account in [Gitlab.com](https://gitlab.com/users/sign_in) 
   - [Create a group](https://docs.gitlab.com/ee/user/group/#create-a-new-group) if desired in GitLab.com and [add group members](https://docs.gitlab.com/ee/user/group/#add-users-to-a-group)
   - Create an account in the [customers portal](https://customers.gitlab.com/customers/sign_in)
   - Associate GitLab.com account with customers portal account. First log into GitLab.com, then in another tab in the same browser open the customers portal and navigate to `My Account` and select `Change Linked Account`
   - Purchase desired subscription from https://about.gitlab.com/pricing/#compare-options, selecting the desired group during the purchase process. 

1. **User doesn't see their group during purchase process.** If the customer can't see their Group when purchasing a subscription, one of the below options are likely happening:
   - They need to create a Group on GitLab.com first
   - They need to associate their GitLab.com account with their customers portal account

## Licensing
Licensing requests refers problems or questions related to license keys for the Self-Managed product. 


1. **Instructions for activating the license key.** For Self-Managed subscriptions, a license key must be uploaded in order to unlock the applicable paid features. Follow the instructions for [uploading the license key](https://docs.gitlab.com/ee/user/admin_area/license.html#uploading-your-license)

1. **User wants to know when they will receive the license key.** The license key will be emailed to the email address associated with the purchase once subscription payment is approved internally by the billing team.  If there is an unusually long delay,  contact the account manager (found in Salesforce)  or ask in the `#deal-desk` Slack channel and cc: the account manager.

1. **User doesn't renew paid Self-Managed plan, what happens to the license and features?** The license key will expire 2 weeks after the expiration date. The system will revert to  read-only mode  if a new key is not uploaded. If  the user doesn't purchase  another plan and  subsequently upload a new license, instruct them to remove the old license so the system will revert to the Core (CE) functionality. Note, it is no longer necessary to downgrade to CE package.

1. **A customer reports problems when registering their license key** Check the points below in order to diagnose the problem:

   1. How many active users are in the GitLab instance of the customer? Does their license key supports that number of active users?
      This is the main issue that we've noted when receiving requests related to problems with registering a license.

      Ask the customer the following:

        > How many active users do you have? (You can find this on the admin dashboard at `http://<hostname>/admin/users` next to the "Active" tab)

   1. If the customer reports the following validation error:

      _During the year before this license started, this GitLab installation had 0 active users, exceeding this license's limit of 5 by -5 users. Please upload a license for at least 0 users or contact sales at renewals@gitlab.com_

      This usually means that the GitLab instance has more active users than the number allowed by the license key. Ask the customer for the number of active users and suggest to buy more seats if required.

       This validation error [has been fixed](https://gitlab.com/gitlab-org/gitlab-ee/merge_requests/4961) but the fix will be included in the `10.7` version of GitLab.

   1.  If the customer reports the following validation error:

        _![You have applied a True-up for 2 users but you need one for 3 users.](https://about.gitlab.com/images/handbook/support/support_license-troubleshooting.png)_

        This usually means that the number of true-up purchased is invalid. If you consider that the customer has purchased an adequate true-up please follow these steps:

            1. Go to the [license app](https://license.gitlab.com/)
            2. Find the last license key by email or company name
            3. Duplicate the license key
            4. Edit the `true up count` field with the correct number
            5. Save the license key

   1. If you can't solve the problem with any of the above steps, submit an issue.

1. **User doesn't understand how true-up works.** At renewal the system will look for the max number of unique users the account has had at any one time. As long as the auto-renew setting is not set to ON, the customer can change the number of users for the upcoming year prior to the renewal. The customer will be charged for the desired number of users for the future subscription as well as the true-up from the previous year.

~~~
    As an example:

    - Year 1, user subscribes for 10 users
    - During the year the account has a max of 12 users
    - At renewal (auto-renew is OFF), the user only wants 5 users for the upcoming year
    - At renewal, the pricing will be for the 5 users for the upcoming year + 2 true-up users for the previous year
~~~

## Billing
Billing refers to inquiries which can be handled by our Accounts Receivable team. Examples of common billing requests are list below. These request should be sent to Accounts Receivable using the `General>Accounts Receivable` macro. 

1. Copy of invoice - _check first if the invoice is available in the [customers portal](https://customers.gitlab.com/customers/sign_in),  if  yes, walk the customer through locating the invoices under Payment History for future self-service ability._
1. Changes to invoice (address, company name, VAT #, etc)  - _before sending this to AR, make sure the customer has provided all of the needed information._
1. Refund requests - _only if the purchase was made within the last 45 days. If outside this period loop in account manager._
1. Requests to make a payment/payment failed - pass the request to AR by selecting `Accounts Receivable` in the form field in the Zendesk ticket.



The following information is helpful to provide to the AR team when transfering tickets, but not required.

1. Subscription #
1. Subscription information - copy & paste from `Manage Purchases` in [customers portal](https://customers.gitlab.com/customers/sign_in)
1. Zuora ID - available in the [customers portal](https://customers.gitlab.com/customers/sign_in)  under the  `Edit` tab
1. Salesforce Account ID - available in the [customers portal](https://customers.gitlab.com/customers/sign_in)  under the  `Edit` tab



## Troubleshooting sales-assisted transactions for GitLab.com

After purchasing through Salesforce, an account is automatically created on customers.gitlab.com with the same email from Salesforce.

1. The customer should to go to https://customers.gitlab.com/customers/password/new and reset their account password
2. After they have logged in, ask them to access the "Subscriptions" menu
3. They'll be able to click onto "Edit" over a subscription
4. They'll be redirected to GitLab.com for OAuth login
5. At this point, they need to make sure they're logging using the account they want to license on GitLab.com
6. Ask them to select the Group they want to license then click "Update"
 

## Additional Reference

1. [Licensing and Subscription Faq](https://about.gitlab.com/pricing/licensing-faq/) _(user facing)_
2. [Subscription Setup and Management](https://docs.gitlab.com/ee/subscriptions/) _(user facing)_
3. [GitLab.com Subscription Management](https://about.gitlab.com/handbook/marketing/product-marketing/enablement/dotcom-subscriptions/) _(internal audience)_
4. [Sales Enablement Reference: GitLab.com Subscriptions](https://about.gitlab.com/handbook/marketing/product-marketing/enablement/dotcom-subscriptions/)
