---
layout: markdown_page
title: "All Remote"
---

## On this page
{:.no_toc}

- TOC
{:toc}

GitLab is an all-remote company with [team members](/company/team/) located in more than 50 countries around the world.

On this page and subpages, we'll share what "all remote" really means, [how it works at GitLab](/company/culture/all-remote/tips/#how-it-works-at-gitlab), some [tips and tricks](/company/culture/all-remote/tips/#tips-for-leaders-and-other-companies) for remote teams, and [resources](/company/culture/all-remote/resources/) to learn more.

## The Remote Manifesto

All-remote work promotes:

1. Hiring and working from all over the world *instead of* from a central location.
1. Flexible working hours *over* set working hours.
1. Writing down and recording knowledge *over* verbal explanations.
1. Written down processes *over* on-the-job training.
1. Public sharing of information *over* need-to-know access.
1. Opening up every document for editing by anyone *over* top-down control of documents.
1. Asynchronous communication *over* synchronous communication.
1. The results of work *over* the hours put in.
1. Formal communication channels *over* informal communication channels.

## Why remote?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/NoFLJLJ7abE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

>  **"Remote is not a challenge to overcome. It's a clear business advantage."** -Victor, Product Manager, GitLab

From the cost savings on office space to more flexibility in employees' daily lives, all-remote work offers a number of advantages to organizations and their people.
But we also recognize that being part of an all-remote company isn't for everyone. Here's a look at some of the advantages and disadvantages.  

## Advantages and benefits

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Xw-31PZkHOo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
Operating in an all-remote environment provides a multitude of benefits and competitive advantages for employees, employers, and the world. 

Learn more about [benefits and advantages to operating in an all-remote environment](/company/culture/all-remote/benefits/).

## Disadvantages

Despite its many [advantages](/company/culture/all-remote/benefits/), all-remote work isn't for everyone. It can have disadvantages for potential employees depending on their lifestyle and work preferences, as well as the organization.

Learn more about [disadvantages to all-remote, along with solutions to these challenges](/company/culture/all-remote/drawbacks/).

## People

All-remote organizations tend to attract people who place a high degree of value on autonomy, flexibility, empathy, and mobility. It also presents outsized opportunity for people who must live or prefer to live in rural areas, where well-paying careers in technical industries are few and far between.

Learn more about the [types of people who are adopting a remote lifestyle](/company/culture/all-remote/people/).

## Jobs

Job seekers are wise to point their efforts towards companies that are built to support 100% remote. You're able to bypass hours of lobbying for a remote arrangement during the interview process, and you're assured that the tools you need to operate effectively from anywhere will be included from the get-go. 

Learn more about [all-remote and remote-first organizations leading the way,  job boards that curate high-quality remote roles, and informal job searching tactics](/company/culture/all-remote/jobs/).

## Management

Managing an all-remote company is much like managing any other company. It comes down to trust, communication, and company-wide support of shared goals.

Learn more about [what's required to effectively and efficiently manage an all-remote company](/company/culture/all-remote/management/).

## Why is this possible now?

All-remote work wouldn't be possible without the constant evolution of technology, and the tools that enable this type of work are continuously being developed and improved.

We aren't just seeing these impacts for all-remote companies. In fact, in some organizations with large campuses, employees will routinely do video calls instead of spending 10 minutes to go to a different building.

Here are some of the key factors that make all-remote work possible:

1. Faster internet everywhere - 100Mb/s+ cable, 5GHz Wifi, 4G cellular
1. Video call software - Google Hangouts, Zoom
1. Mobile technology - Everyone has a computer in their pocket
1. Evolution of speech-to-text conversion software - more accurate and faster than typing
1. Messaging apps - Slack, Mattermost, Zulip
1. Issue trackers - Trello, GitHub issues, GitLab issues
1. Virtual workspace tools - Remo
1. Suggestions - GitHub Pull Requests, GitLab Merge Requests
1. Static websites - GitHub Pages, GitLab Pages
1. English proficiency - More people are learning English
1. Increasing traffic congestion in cities
1. More demand for flexibility from new professionals entering the workforce

## What "all remote" does not mean

Let's address some of the common misconceptions about all-remote work.

First things first: An all-remote company means there is *no* office where multiple people are based. 
The only way to not have people in a satellite office is not to have a main office. 
It's not that we don't have a headquarters, it is that we have 800+ (and growing) headquarters!

The terms "remote" and "distributed" are often used interchangeably, but they're not quite the same. We prefer the term "remote" because "distributed" suggests multiple physical offices.
"Remote" is also the [most common term](https://www.google.com/search?ei=4IBsXKnLDIGRggftuqfAAQ&q=distributed+companies&oq=distributed+companies&gs_l=psy-ab.12...0.0..5177...0.0..0.0.0.......0......gws-wiz.6xnu76aJWr4) to refer to the absence of a physical workspace, and being able to do your job from anywhere.

For employees, being part of an all-remote company does not mean working independently or being isolated, because it's not a substitute for human interaction.
Technology allows us to [stay closely in touch](company/culture/all-remote/tips/#we-facilitate-informal-communication) with our teams, whether asychronously in text or in real time with high-fidelity conversations through video.
Teams should collaborate closely, communicate often, [build relationships virtually](/2019/07/31/pyb-all-remote-mark-frein/), and feel like valuable members of a larger team.

Working remotely also doesn't mean you're physically constrained to home.
You're free to work wherever you want. That could be at home with family, a coffee shop, a coworking space, or your local library while your little one is enjoying storytime. It could mean that you're location independent, traveling around and working in a new place each week.
You can have frequent video chats or virtual pairing sessions with co-workers throughout the day, and you can even meet up with other coworkers to work together in person if you're located near each other.

At the organizational level, "all-remote" does not mean simply offshoring work. Instead, it means you're able to hire the best talent from all around the world.
It's also not a management paradigm. You still have a hierarchical organization, but with a focus on output instead of input.

All in all, remote is fundamentally about _freedom_ and _individual choice_. At GitLab, we [value your results](/handbook/values/#results), not where you get your work done.

## Our long-term vision for remote work

There are a few important outcomes we expect to see as remote work becomes even more prevalent around the world:

1. The majority of new startups intentionally forming as all-remote companies.
1. Cities in developing countries, particularly in Africa, enabled by all-remote jobs at companies founded by local leaders.
1. Most startups in the Bay Area with a significant portion of their workforce working remotely.
1. Increased wages for remote work outside of metro areas.

## How we built our all-remote team

As GitLab has grown, we've learned a lot about what it takes to build and manage a fully remote team, and want to share this knowledge to help others be successful.

Find out [how GitLab makes it work](/company/culture/all-remote/tips/#how-it-works-at-gitlab).

## Tips for working remotely

Building a remote team or starting your first all-remote job? Check out our [tips for working remotely.](/company/culture/all-remote/tips)

## Resources

Browse our [resources page](/company/culture/all-remote/resources) to learn more about GitLab's approach, read about remote work in the news, and see what other companies are leading the way.

We've also compiled a [list of companies](/handbook/got-inspired/) that have been inspired by GitLab's culture.

## Hiring

GitLab envisions a world where talented, driven individuals can find roles and seek employment based on business needs, rather than an oftentimes arbitrary location. 

Hiring across the globe isn't without its challenges. There are local regulations and risks unique to countries and regions around the globe. We believe that these challenges are worth overcoming, and opening our recruiting pipeline beyond the usual job centers creates a competitive advantage. We hope to see this advantage wane as more all-remote companies are created. 

Learn more about [hiring in an all-remote environment](/company/culture/all-remote/hiring/).

## Compensation

While there are certain complexities to paying team members who are spread out in over 50 countries, we believe that it's worthwhile. Being an all-remote company enables us to [hire the world's best talent](/company/culture/all-remote/hiring/), not just the best talent from a few cities.

Learn more about [compensation in an all-remote environment](/company/culture/all-remote/compensation/).

## Learning and Development

We believe that all-remote companies are at a competitive advantage when it comes to educating and developing team members.

Learn more on how to make [learning and development a companywide mindset in an all-remote environment](/company/culture/all-remote/learning-and-development/).

## Informal Communication

In an all-remote environment, informal communication should be formally addressed. Leaders should organize informal communication, and to whatever degree possible, design an atmosphere where team members all over the globe feel comfortable reaching out to anyone to converse about topics unrelated to work.

Learn more about [enabling informal communication in an all-remote company](/company/culture/all-remote/informal-communication/).

## Meetings

Learn how to decide when a meeting is necessary and [how to optimize them in an all-remote environment](/company/culture/all-remote/meetings/). 

## Stories

Read the [stories](/company/culture/all-remote/stories/) of some of our team members and hear how remote work has impacted their lives.

## Interviews

Read and listen to [interviews](/company/culture/all-remote/interviews/) on the topic of working remotely, hosted by GitLab team members. 

## History

Learn about [historical milestones, inflection points, and prescient interviews](/company/culture/all-remote/history/) in the evolution and expansion of remote work.

## Part-remote

Part-remote companies have one or more offices where a subset of the company commutes to each day, paired with a subset of the company that works remotely. 

Learn more about [advantages, challenges, and the primary differences between all-remote and other forms of remote working](/company/culture/all-remote/part-remote/).

## Remote work conferences, summits, and events

Particularly for those who are seeking a new role with an all-remote or remote-first company, events can be a great place to meet others who have experience and connections in the space.

Learn more about [remote work conferences and summits, the power of networking, and the unique elements of experiencing a virtual event](/company/culture/all-remote/events/).

## All-remote Pick Your Brain interviews

If people want advice on structuring or managing an all-remote organization, we'll consider it. Learn more about [requesting a Pick Your Brain interview on all-remote](/company/culture/all-remote/pick-your-brain/).

## Contribute to this page

At GitLab, we recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community.
Here's how you can participate:

- Propose or suggest any change to this site by creating a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.
